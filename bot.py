import telebot
import requests
import re

from datetime import datetime, timedelta

import config

from bs4 import BeautifulSoup

bot = telebot.TeleBot(config.token)


def get_html(params_get):
    response = requests.post('http://195.95.232.162:8082/cgi-bin/timetable.cgi?n=700', data=params_get)
    response.encoding = 'windows-1251'
    return response.text


def get_parameters(faculty=' ', teacher='', group='', sdate='', edate=''):
    parameters = {
        #'Content-type': 'application/x-www-form-urlencoded',
        'faculty': faculty,
        'teacher': teacher,
        'group': group,
        'sdate': sdate,
        'edate': edate,
    }
    return parameters


def parse(html):
    soup = BeautifulSoup(html, 'lxml')
    table_all = soup.find_all('table', class_="table table-bordered table-striped")
    data = []
    for table in table_all:
        # table = soup.find('table', class_="table table-bordered table-striped")
        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            cols = [ele.text.strip() for ele in cols]
            data.append([ele for ele in cols if ele])
    # data_str = None
    data_str = ('\n'.join('\n'.join(elems) for elems in data))
    # print(data_str)
    return data_str
    # print(data_str)


def main(params):
    get_html_utf = u''.join(get_html(params))
    return parse(get_html_utf)


@bot.message_handler(commands=['start'])
def send_welcome(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('/start', '/today')
    user_markup.row('/tomorrow')
    user_markup.row('/Zaspa_today')
    bot.send_message(message.from_user.id, "Hi, i am schedule bot_CHDTU, suk. "
                                           "\n You must write date in format : '2.03.2017 4.05.2017'"
                                           " or press any button",
                     reply_markup=user_markup)


@bot.message_handler(commands=['today'])
def send_msg_today(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('/start', '/today')
    user_markup.row('/tomorrow')
    user_markup.row('/Zaspa_today')
    format_date = datetime.strftime(datetime.now(), "%d.%m.%Y")
    group = u'пз-144'.encode('windows-1251')
    sdate = format_date.encode('windows-1251')
    params = get_parameters (faculty='', teacher='', group=group, sdate=sdate, edate='')
    try:
        bot.send_message(message.from_user.id, main(params), reply_markup=user_markup)
    except telebot.apihelper.ApiException:
        bot.send_message(message.from_user.id, "There is no schedule for this date. "
                                               "Try other date", reply_markup=user_markup)




@bot.message_handler(commands=['tomorrow'])
def send_msg_tomorrow(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('/start', '/today')
    user_markup.row('/tomorrow')
    user_markup.row('/Zaspa_today')
    one_day = timedelta(days=1)
    tommorow = datetime.now()+one_day
    format_date = datetime.strftime(tommorow, "%d.%m.%Y")
    group = u'пз-144'.encode('windows-1251')
    sdate = format_date.encode('windows-1251')
    params = get_parameters(faculty='', teacher='', group=group, sdate=sdate, edate='')
    try:
        bot.send_message(message.from_user.id, main(params), reply_markup=user_markup)
    except telebot.apihelper.ApiException:
        bot.send_message(message.from_user.id, "There is no schedule for this date. "
                                               "Try other date", reply_markup=user_markup)


@bot.message_handler(commands=['Zaspa_today'])
def send_msg_tomorrow(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('/start', '/today')
    user_markup.row('/tomorrow')
    user_markup.row('/Zaspa_today')
    format_date = datetime.strftime(datetime.now(), "%d.%m.%Y")
    teacher = u'Заспа Григорій Олександрович'.encode('windows-1251')
    sdate = format_date.encode('windows-1251')
    params = get_parameters(faculty='', teacher=teacher, group='', sdate=sdate, edate='')
    try:
        bot.send_message(message.from_user.id, main(params), reply_markup=user_markup)
    except telebot.apihelper.ApiException:
        bot.send_message(message.from_user.id, "There is no schedule for this date. "
                                               "Try other date", reply_markup=user_markup)


@bot.message_handler(func=lambda message: True, content_types=['text'])
def answer_to_pz(message):
    user_markup = telebot.types.ReplyKeyboardMarkup(True, True)
    user_markup.row('/start', '/today')
    user_markup.row('/tomorrow')
    user_markup.row('/Zaspa_today')
    list_date = re.split(r' ', message.text)
    try:
        start_date = list_date[0]
        group = u'пз-144'.encode('windows-1251')
        sdate = start_date.encode('windows-1251')
        params = get_parameters(faculty='', teacher='', group=group, sdate=sdate, edate='')
        bot.send_message(message.from_user.id, main(params), reply_markup=user_markup)
    except IndexError:
        end_date = list_date[0]
        group = u'пз-144'.encode('windows-1251')
        sdate = start_date.encode('windows-1251')
        edate = end_date.encode('windows-1251')
        params = get_parameters(faculty='', teacher='', group=group, sdate=sdate, edate=edate)
        bot.send_message(message.from_user.id, main(params), reply_markup=user_markup)
    except telebot.apihelper.ApiException:
        bot.send_message(message.from_user.id, "There is no schedule for this date. "
                                               "Try other date", reply_markup=user_markup)


if __name__ == '__main__':
    bot.polling(none_stop=True)